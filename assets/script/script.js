// DOM
// Document Object Model
// Document
// console.log(document);

// DOM Manipulation means, we can access any part of our DOM using Javascript

// How do we access specific parts of our DOM?

// Access an element via ID

// const h1 = document.getElementById('mainHeader');
// console.log()

// // to change TEXT CONTENT
// h1.textContent = "Bye World!";

// // to change text content and HTML
// h1.innerHTML = `Bye + <span class="text-danger"> World! </span>`;

// const lis = document.getElementsByClassName('color');
	// console.log(lis);

// this will return an HTML Collection
// An HTML Collection is an array-like type of data.
// // This represents the elements.
// And we can only access it via its index.
// Although it is array-like, WE CAN'T USE ARRAY LOOP METHODS.
// x forEach, map, filter
// o for loop/ can use
// for(let index=0); index < lis.length; index++){
// 	console.log(lis[index]);

// }; --> not error

// get elements via their tags
// this will also return an html collection

// const uls = document.getElementsByTagName('ul');

// console.log(uls);

// QuerySelector
// returns nodelist intead of HTML Collection
// this includes all types od nodes like elements, text nodes,attributes

// query selector accepts 
// const h1 = document.querySelector('#mainHeader');
// console.log(h1);

// const li = document.querySelector('.color');
// console.log(li);

// const lis = document.querySelectorAll('li');
// console.log(lis);

// A node list is also an array like type of data
// we can access individual data via its index
// unlike HTML collection we can use array loop methods in node list.

// lis.forEach(function(indicli){
// 	console.log(indiv.Li);
// }); --> will work