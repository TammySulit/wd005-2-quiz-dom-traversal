// DOM TRAVERSAL QUIZ
// 	Quiz:
// 	From h1 with ID Main-Title

// 	Target the Following Elements
// 		1. td - Benguet
// 		2. td - Aklan
// 		3. Body
// 		4. Thead
// 		5. td - Palawan
// 		6. th - Spot
// 		7. td - 2
// ===================================================
// ANSWER SECTION
	
// 1.h1.parentElement.nextElementSibling
// .lastElementChild.lastElementChild.lastElementChild

// 2.h1.parentElement.nextElementSibling
// .lastElementChild.lastElementChild
// .previousElementSibling.lastElementChild;

// 3.h1.parentElement.parentElement.parentElement

// 4.h1.parentElement.nextElementSibling
// .firstElementChild

// 5.h1.parentElement.nextElementSibling
// .lastElementChild.firstElementChild.lastElementChild

// 6.h1.parentElement.nextElementSibling
// .firstElementChild.firstElementChild
// .firstElementChild.nextElementSibling

// 7.h1.parentElement.nextElementSibling
// .lastElementChild.firstElementChild
// .nextElementSibling.firstElementChild
