//DOM
//Document Object Model
//Document--- 
// console.log(document);

//DOM Manipulation means, we can access any part of our DOM using Javascript.
//
//How do we access specific parts of our DOM?
//
//Access an element via ID

// const h1 = document.getElementById('mainHeader');
//to change TEXT CONTENT
// h1.textContent = "Bye World!";

//to change text content AND html
// h1.innerHTML = `Bye <span class="text-danger">World!</span>`; 

// to change style
// h1.style.color = 'salmon';
// h1.style.fontSize = '250px';
//In javascript, css properties are written in camel case.

// to modify class
// To modify class, remember that we need to access first the classlist of the element.
//to add a class
// h1.classList.add('text-danger');
// to remove a class
// h1.classList.remove('text-danger');
// 
//get elements via their class names

// const lis = document.getElementsByClassName('color');
// console.log(lis);
//this will return an HTML Collection
//Includes elements only
//An HTML Collection is an array-like type of data.
//This represents the elements.
//And we can only access it via its index.
//Although it is array-like, WE CAN'T use array loop methods.
// x forEach, map, filter //can't use
// lis.forEach(function(indivLi){
//     console.log(indivLi);
// }); --> error
// 
// o for loop // can use
// for(let index=0; index < lis.length; index ++){
//     console.log(lis[index]);
// }; --> not error
// 
//get elements via their tags
//this will also return an html colleciton
// const uls = document.getElementsByTagName('ul');
// console.log(uls);

//QuerySelector
//returns nodelist instead of HTML Collection
//this includes all types of nodes like elements, text nodes, attributes

//query selector accepts css selector as a parameter
//querySelector will return the first element that will satisfy the parameter
// const h1 = document.querySelector('#mainHeader');
// console.log(h1);

// const li = document.querySelector('.color');
// console.log(li);
// 
// querySelectorAll
// It will return all elements that will satisfy the parameter
const lis = document.querySelectorAll('li');
console.log(lis);
//A node list is also an array-like type of data.
//we can access individual data via its index.
//unlike HTML collection, we can use array loop methods in node list.
//lis.forEach(function(indivLi){
//     console.log(indivLi);
// }); ---> will work

// domtraversal.js
// FIRST RULE:
// CODE PROPERLY. INDENT PROPERLY!!!!!!!!!!!
// Traversal from top --> down
// 
//Task: target tbody via id.
//Solution: we can use either document.getElementById or document.querySelector

const tbody = document.getElementById('listBody'); //top element
//const tbody = documnet.querySelector('#listBody');
//
//to access first child
//tbody.firstElementChild;
//
//to access last child
//tbody.lastElementChild;
//
//traversal bottom--> up;
//to access parent 
//tbody.parentElement;
//
//traversal same-parent
//tbody.previousElementSibling;
//
const thead = document.getElementById('listHead');

//thead.nextElementSibling;
